/**
 * University of San Diego
 * COMP 285: Spring 2015
 * Instructor: Gautam Wilkins
 *
 * Main class to run a single game of Connect Four between two opponents.
 */
public class Main {

    public static void main(String[] args) {

        int sum = 0;
        int total = 10;
        for (int i = 0; i < total; i++) {
            Player p1 = new Player1();
            Player p2 = new Player1();

            Game gameState = new Game(p1, p2);
            int winner = gameState.startGame(false);
            sum += winner-1;
        }
        System.out.println("Player 1: " + (total - sum) + "\n" +
                "Player 2: " + sum);
    }
}
