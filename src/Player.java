/**
 * University of San Diego
 * COMP 285: Spring 2015
 * Instructor: Gautam Wilkins
 *
 * Abstract class for a Connect Four player.
 */
public abstract class Player {
    public int playerNumber;

    abstract public int chooseMove(Board gameBoard);
    abstract public void setPlayerNumber(int number);
}
