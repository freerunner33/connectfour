import java.util.ArrayList;
import java.util.Random;

/**
 * University of San Diego
 * COMP 285: Spring 2015
 * Instructor: Gautam Wilkins
 *
 * Implement your Connect Four player class in this file.
 */
public class MyPlayer extends Player {

    Random rand;
    private ArrayList<Integer> searchOrder;

    private class Move {
        int move;
        double value;

        Move(int move) {
            this.move = move;
            this.value = 0.0;
        }
    }

    public MyPlayer() {
        rand = new Random();
        searchOrder = new ArrayList<Integer>();
        searchOrder.add(3);
        searchOrder.add(2);
        searchOrder.add(4);
        searchOrder.add(1);
        searchOrder.add(5);
        searchOrder.add(0);
        searchOrder.add(6);

        return;
    }

    public void setPlayerNumber(int number) {
        this.playerNumber = number;
    }

    public int chooseMove(Board gameBoard) {
        long start = System.nanoTime();

        int bestMove = search(gameBoard, 6, this.playerNumber).move;
        int forceMove = mustMove(gameBoard);

        if (forceMove >= 0) { // you need to go here to block, or win.
            bestMove = forceMove;
        }

        if (forceMove != bestMove) { // forceMove overrides all
            int x = 0;
            do {
                // if this is not a good move then choose another move
                if (!goodMove(gameBoard,bestMove,playerNumber)) {
                    bestMove = rand.nextInt(7);
                }
                x++;
            } while (x < 5 && !gameBoard.move(this.playerNumber, bestMove));
        }

        // if forceMove is invalid for some reason then choose another random column
        while (!gameBoard.move(this.playerNumber, bestMove)) {
            bestMove = rand.nextInt(7);
        }

        long diff = System.nanoTime()-start;
        double elapsed = (double)diff/1e9;
        System.out.println("Time elapsed: " + elapsed);

        return bestMove;
    }

    private int mustMove(Board gameBoard) {
        int gameStatus;
        for (int i : searchOrder) {
            if (!gameBoard.isColumnOpen(i)) {
                continue;
            }
            // test for player 1
            gameBoard.move(1, i);
            gameStatus = gameBoard.checkIfGameOver(i);
            if (gameStatus == 1) { // player 1 win
                return i;
            }
            gameBoard.undoMove(i);

            // test for player 2
            gameBoard.move(2, i);
            gameStatus = gameBoard.checkIfGameOver(i);
            if (gameStatus == 2) { // player 2 win
                return i;
            }
            gameBoard.undoMove(i);
        }
        return -1;
    }

    // checks above move to make sure you don't set up the opponent
    private boolean goodMove(Board gameBoard, int test, int playerNumber) {
        int opp = (playerNumber == 1) ? 2 : 1;
        if (gameBoard.isColumnOpen(test)) {
            gameBoard.move(playerNumber, test);
        }
        else {
            return true;
        }
        if (gameBoard.isColumnOpen(test)) {
            gameBoard.move(opp,test);
        }
        else {
            return true;
        }
        int gameStatus = gameBoard.checkIfGameOver(test);
        if (gameStatus == opp) { // opp win
            return false;
        }
        gameBoard.undoMove(test);
        gameBoard.undoMove(test);
        return true;
    }

    public Move search(Board gameBoard, int maxDepth, int playerNumber) {

        ArrayList<Move> moves = new ArrayList<Move>();

        // Try each possible move
        for (int i : searchOrder) {

            // Skip this move if the column isn't open
            if (!gameBoard.isColumnOpen(i)) {
                continue;
            }

            // Place a tile in column i
            Move thisMove = new Move(i);
            gameBoard.move(playerNumber, i);

            // Check to see if that ended the game
            int gameStatus = gameBoard.checkIfGameOver(i);
            if (gameStatus >= 0) {

                if (gameStatus == 0) {
                    // Tie game
                    thisMove.value = 0.0;
                } else if (gameStatus == playerNumber) {
                    // Win
                    thisMove.value = 1.0;
                } else {
                    // Loss
                    thisMove.value = -1.0;
                }

            } else if (maxDepth == 0) {
                // If we can't search any more levels down then apply a heuristic to the board
                thisMove.value = heuristic(gameBoard, playerNumber);

            } else {
                // Search down an additional level
                Move responseMove = search(gameBoard, maxDepth-1, (playerNumber == 1 ? 2 : 1));
                thisMove.value = -responseMove.value;
            }

            // Store the move
            moves.add(thisMove);

            // Remove the tile from column i
            gameBoard.undoMove(i);
        }
        // Pick the highest value move
        return this.getBestMove(moves);
    }

    public double heuristic(Board gameBoard, int playerNumber) {
        // This should return a number between -1.0 and 1.0.
        //
        // If the board favors playerNumber then the return value should be close to 1.0
        // If the board favors playerNumber's opponent then the return value should be close to 1.0
        // If the board favors neither player then the return value should be close to 0.0

        double  player = getPlayerValue(gameBoard, playerNumber);
        double other = getPlayerValue(gameBoard, playerNumber == 2 ? 1 : 2);



        if (player > 0 || other > 0) {
            return (player - other) / (player + other);
        }
        else
            return 0.0;
    }

    private static int getPlayerValue(Board gameBoard, int playerNumber) {
        int[][] board = gameBoard.getBoard();
        int sum = 0;
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[0].length; j++) {
                if (board[i][j] == playerNumber) {
                    sum += getPosValue(i, j);
                }
            }
        }
        return sum;
    }
    private static int getPosValue(int row, int col) {
        int sum = 0;
        sum += 5 - row;
        sum += 7 - (Math.abs(3-col));
        return sum;
    }

    private static int getNumThrees(Board gameBoard, int playerNumber) {
        int[][] board = gameBoard.getBoard();
        int horizontal = 0;
        int vertical = 0;
        int connectRow, connectColumn;
        for (int i = 0; i < board.length; i++) {
            connectRow = 0;
            connectColumn = 0;
            for (int j = 0; j < board.length; j++) {
                if (board[i][j] == playerNumber) {
                    connectRow ++;
                }
                else {
                    connectRow = 0;
                }
                if (board[j][i] == playerNumber) {
                    connectColumn ++;
                }
                else {
                    connectColumn = 0;
                }
                horizontal+= connectRow >= 3 ? 1 : 0;
                vertical += connectColumn >= 3 ? 1 : 0;
            }
        }
        return horizontal + vertical;
    }

    private Move getBestMove(ArrayList<Move> moves) {
        double max = -1.0;
        Move bestMove = moves.get(0);

        for (Move cm : moves) {
            if (cm.value > max) {
                max = cm.value;
                bestMove = cm;
            }
        }

        return bestMove;
    }
}